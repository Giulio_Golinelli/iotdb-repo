-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Sat May  9 18:21:17 2020 
-- * LUN file: D:\programmazzione\DBMain\sensori\iot.lun 
-- * Schema: IOT_LOGIC/1 
-- ********************************************* 


-- Emittionbase Section
-- ________________ 

create database IOTDB;
use IOTDB;


-- Tables Section
-- _____________ 

create table Area (
     ID int not null AUTO_INCREMENT,
     Name varchar(15) not null,
     primary key (ID));

create table Sensor (
     ID int not null AUTO_INCREMENT,
     Connected int default null,
     Battery int not null default 100,
     Area int not null,
     Description varchar(25) not null,
     Kind enum('Meter', 'Detector', 'Receiver') not null default 'Receiver',
     Unit char(3) default NULL,
     primary key (ID),
     unique (Connected),
     constraint battery check(Battery<=100 and Battery>=0),
     constraint invalid_state check(
          (Kind='Detector' AND Unit is null) 
          OR (Kind='Meter' AND Unit is not null) 
          OR (Kind='Receiver' AND Connected is null AND Unit is null)),
     constraint sensor_connected foreign key (Connected) references Sensor (ID) on delete restrict,
     constraint sensor_area foreign key (Area) references Area (ID) on delete cascade);
     
create table Emittion (
     SensorID int not null,
     Emittion_Time datetime not null default NOW(),
     Emittion_Value int not null,
     primary key (SensorID, Emittion_Time),
     constraint emittion_sensor foreign key (SensorID) references Sensor (ID) on delete cascade);

-- Index Section
-- _____________

-- Input Section
-- _____________

#Insert every Area
INSERT INTO Area (Name) VALUES ('Area1'), ('Area2'), ('Area3'), ('Area4');

# Insert one Receiver for every Area
INSERT INTO Sensor (Area, Description) VALUES 
     (1, "Receiver Area1"), 
     (2, "Receiver Area2"), 
     (3, "Receiver Area3"), 
     (4, "Receiver Area4");

# Insert one fire detector for first three areaa
INSERT INTO Sensor (Connected, Area, Kind, Description) VALUES 
     (1, 1, "Detector", "Fire Detector"), 
     (2, 2, "Detector", "Fire Detector"), 
     (3, 3, "Detector", "Fire Detector");

# connect to every fire detector a thermometer
# and a Thermometer to a Thermometer
INSERT INTO Sensor (Connected, Area, Description, Kind, Unit) VALUES 
     (5, 1, "Thermometer", "Meter", "C"),
     (6, 2, "Thermometer", "Meter", "C"), 
     (7, 3, "Thermometer", "Meter", "C"), 
     (8, 1, "Thermometer", "Meter", "C");

#A new FireDetector not connected in area 4
INSERT INTO Sensor(Area, Kind, Description) VALUES (4, 'Detector', 'FireDetector');

#Insert some data
INSERT INTO Emittion (SensorID, Emittion_Time, Emittion_Value) VALUES (9, NOW(),  24), (9, '2020-01-01 23:59:59', 26), (10, NOW(), 32);


-- Stored Objects Section
-- _____________

DELIMITER $$

#Prende i più recenti N dati da ciascun sensore Meter o Detector
CREATE PROCEDURE last_emittions(in n integer)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK; 
		RESIGNAL;
	END;

     START TRANSACTION;
     DROP TEMPORARY TABLE IF EXISTS last_emittions;
     CREATE TEMPORARY TABLE last_emittions(
          SensorID int not null,
          Emittion_Time datetime not null default NOW(),
          Emittion_Value int not null,
          Unit varchar(3),
          primary key (SensorID, Emittion_Time)
     ) ENGINE=MEMORY AS (
          SELECT SensorID, Emittion_Time, Emittion_Value, Unit
          FROM (
               SELECT *, RANK() OVER (PARTITION BY SensorID ORDER BY Emittion_Time DESC) AS rnk
               FROM Emittion
          ) AS x, Sensor
          WHERE rnk <= n AND SensorID = Sensor.ID AND (Kind="Meter" OR Kind="Detector")
     );
     COMMIT;
     SELECT * FROM last_emittions;
END $$

/*
Emette un nuovo valore per i sensori che sono connessi e che sono 'Meter' o 'Detector'.
Se il sensore è di tipo meter ed è nuovo, allora immette un valore tra 20 e 30 
altrimenti emette l'ultimo valore emesso da quel sensore +- 4.
Se il sensore è di tipo detector ci sono 3/10 di possibilità di emettere un valore positivo.
*/
CREATE PROCEDURE sensor_emit()
BEGIN
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE SensorID INT;
     DECLARE Connected INT;
	DECLARE Kind ENUM('Meter', 'Detector');
    DECLARE Battery INT;
	DECLARE Emittion_Value INT;
     DECLARE new_value INTEGER;
	DECLARE sensor_last_emit_cursor CURSOR FOR SELECT Sensor.ID, Sensor.Connected, Sensor.Battery, Sensor.Kind, last_emittions.Emittion_Value 
												FROM Sensor LEFT OUTER JOIN last_emittions ON Sensor.ID = last_emittions.SensorID
												WHERE Sensor.Kind = 'Meter' OR Sensor.Kind = 'Detector';
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

	OPEN sensor_last_emit_cursor;
	sensor_last_emit_loop: LOOP
		FETCH sensor_last_emit_cursor into SensorID, Connected, Battery, Kind, Emittion_Value;
		IF finished = 1 THEN 
			LEAVE sensor_last_emit_loop;
		END IF;
          IF Connected IS NOT NULL AND Battery > 0 THEN
               IF Kind = 'Meter' THEN
                    IF Emittion_Value IS NULL THEN
                         INSERT INTO Emittion VALUES (SensorID, NOW(), FLOOR(RAND()*(30-20+1))+20);
                    ELSE
                         INSERT INTO Emittion VALUES (SensorID, NOW(), Emittion_value+4*FLOOR(RAND()*(1+1+1)-1));
               END IF;
               ELSEIF Kind = 'Detector' AND RAND() >= 0.7 THEN
                    INSERT INTO Emittion VALUES (SensorID, NOW(), 1);
               END IF;
          END IF;
	END LOOP;
END $$

# Trigger che previene l'inserimento di un sensor di una Area X collegato a un sensore di una area Y
create trigger connected_trigger
before insert on Sensor
for each row 
begin
    DECLARE connected_Area INT;
    
    if new.Connected is not null then
		SELECT Area into connected_Area
		From Sensor
		WHERE ID = new.Connected;
		
		if new.Area <> Connected_Area then
			signal sqlstate '45000';
		end if;
	end if;		
END $$

# Trigger che rileva se si sta inserendo un receiver in una area nel caso ne esista già uno
create trigger receiver_trigger
before insert on Sensor
for each row 
begin
    DECLARE receiver INT;
    
    if new.Kind = "receiver" then
		SELECT ID into receiver
		From Sensor
		WHERE Kind = "receiver"
		AND Area = new.Area;
		
		if receiver is not null then
			signal sqlstate '45000';
		end if;
	end if;		
END $$

# genera nuove emissioni per i sensori ogni 5 minuti
/*
CREATE EVENT sensors_periodic_emittions_event
     ON SCHEDULE EVERY 5 MINUTE
     DO
          BEGIN
               CALL last_emittions(1);
               CALL sensor_emit();
          END $$
*/
DELIMITER ;

#Diminuisce di un punto percentuale tutti i sensori che non sono 'receiver' ogni giorno
CREATE EVENT sensors_periodic_discharge_event
	ON SCHEDULE EVERY 1 DAY
	DO
		UPDATE Sensor SET Battery = Battery - 1 WHERE Kind <> 'Receiver';

# Procedura per capire il collegamento di un sensore alla radice
CREATE PROCEDURE sensor_path (in initial_sensor_id integer)
	WITH RECURSIVE s_path AS(
		SELECT ID, Description, Connected
		FROM Sensor
		WHERE ID = initial_sensor_id
		UNION ALL
		SELECT s.ID, s.Description, s.Connected
		FROM Sensor s INNER JOIN s_path sp ON sp.Connected = s.ID)
	SELECT *
	FROM s_path;

-- Views Section
-- _____________

CREATE VIEW sensor_info
AS
SELECT Sensor.ID, Sensor.Description, Sensor.Kind, Sensor.Area, Sensor.Battery
FROM Sensor;

CREATE VIEW area_info AS
	SELECT *
	FROM Area;

