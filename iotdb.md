<p style="text-align: right; font-style: italic; font-size:150%; margin: auto">
	Giulio Golinelli<br>
	0000883007<br>
	14/06/2020	
</p>

<h1 style="text-align: center">IOTDB</h1>

Internet Of Things DataBase è un sistema informativo *sql based* per la gestione di sensori IOT.

<!-- omit in toc -->
# Table of contents
- [1. Premessa](#1-premessa)
- [2. Analisi dei requisiti](#2-analisi-dei-requisiti)
	- [2.1. Intervista](#21-intervista)
	- [2.2. Rilevamento delle ambiguità e correzioni proposte](#22-rilevamento-delle-ambiguità-e-correzioni-proposte)
		- [2.2.1. Connettività e Libertà di un sensore](#221-connettività-e-libertà-di-un-sensore)
	- [2.3. Definizione delle specifiche ed estrazione dei concetti principali](#23-definizione-delle-specifiche-ed-estrazione-dei-concetti-principali)
- [3. Progettazione concettuale](#3-progettazione-concettuale)
	- [3.1. Schema scheletro e raffinamenti proposti](#31-schema-scheletro-e-raffinamenti-proposti)
		- [3.1.1. Area e componente](#311-area-e-componente)
		- [3.1.2. Sensori](#312-sensori)
		- [3.1.3. Connessione](#313-connessione)
		- [3.1.4. Emissione](#314-emissione)
	- [3.2. Schema finale](#32-schema-finale)
- [4. Progettazione logica](#4-progettazione-logica)
	- [4.1. Stima del volume dei dati](#41-stima-del-volume-dei-dati)
	- [4.2. Descrizione delle operazioni principali e stima della loro frequenza](#42-descrizione-delle-operazioni-principali-e-stima-della-loro-frequenza)
	- [4.3. Schemi di navigazione e tabelle degli accessi](#43-schemi-di-navigazione-e-tabelle-degli-accessi)
		- [4.3.1. **OP 1**: Visualizzazione delle aree disponibili](#431-op-1-visualizzazione-delle-aree-disponibili)
		- [4.3.2. **OP 2**: Visualizzazione dei componenti con le relative informazioni](#432-op-2-visualizzazione-dei-componenti-con-le-relative-informazioni)
		- [4.3.3. **OP 3**: Data un'area, visualizazzione del possibile componente a cui un sensore potrebbe collegarsi](#433-op-3-data-unarea-visualizazzione-del-possibile-componente-a-cui-un-sensore-potrebbe-collegarsi)
		- [4.3.4. **OP 4**: Aggiunta di un sensore di tipo Meter o Detector](#434-op-4-aggiunta-di-un-sensore-di-tipo-meter-o-detector)
		- [4.3.5. **OP 5**: Ricezione e conservazione nello storico di una emissione di un sensore](#435-op-5-ricezione-e-conservazione-nello-storico-di-una-emissione-di-un-sensore)
		- [4.3.6. **OP 6**: Visualizazzione delle ultime 3 emissioni di un sensore](#436-op-6-visualizazzione-delle-ultime-3-emissioni-di-un-sensore)
		- [4.3.7. **OP 7**: visualizzare la catena dei sensori collegati fino al receiver](#437-op-7-visualizzare-la-catena-dei-sensori-collegati-fino-al-receiver)
	- [4.4. Raffinamento dello schema](#44-raffinamento-dello-schema)
		- [4.4.1. Eliminazione delle gerarchie](#441-eliminazione-delle-gerarchie)
		- [4.4.2. Eliminazione degli identificatori esterni](#442-eliminazione-degli-identificatori-esterni)
		- [4.4.3. Scelta delle chiavi primarie](#443-scelta-delle-chiavi-primarie)
	- [4.5. Schema relazione finale](#45-schema-relazione-finale)
- [5. Schema fisico](#5-schema-fisico)
	- [5.1. Introduzione](#51-introduzione)
	- [5.2. Views](#52-views)
		- [5.2.1. component_info](#521-component_info)
		- [5.2.2. area_info](#522-area_info)
	- [5.3. Stored procedures](#53-stored-procedures)
		- [5.3.1. last_emittions](#531-last_emittions)
		- [5.3.2. sensor_emit](#532-sensor_emit)
		- [5.3.3. component_path](#533-component_path)
	- [5.4. Events](#54-events)
		- [5.4.1. sensor_periodic_discharge_event](#541-sensor_periodic_discharge_event)
		- [5.4.2. sensor_periodic_emittions_event](#542-sensor_periodic_emittions_event)
	- [5.5. Triggers](#55-triggers)
		- [5.5.1. connected_trigger](#551-connected_trigger)
		- [5.5.2. receiver_trigger](#552-receiver_trigger)
	- [5.6. Traduzione delle operazioni in query sql](#56-traduzione-delle-operazioni-in-query-sql)
		- [5.6.1. **OP 1**: Visualizzazione delle aree disponibili](#561-op-1-visualizzazione-delle-aree-disponibili)
		- [5.6.2. **OP 2**: Visualizzazione dei componenti con le relative informazioni](#562-op-2-visualizzazione-dei-componenti-con-le-relative-informazioni)
		- [5.6.3. **OP 3**: Data un'area, visualizazzione del possibile componente a cui un sensore potrebbe collegarsi](#563-op-3-data-unarea-visualizazzione-del-possibile-componente-a-cui-un-sensore-potrebbe-collegarsi)
		- [5.6.4. **OP 4**: Aggiunta di un sensore di tipo Meter o Detector](#564-op-4-aggiunta-di-un-sensore-di-tipo-meter-o-detector)
		- [5.6.5. **OP 5**: Ricezione e conservazione nello storico di una emissione di un sensore](#565-op-5-ricezione-e-conservazione-nello-storico-di-una-emissione-di-un-sensore)
		- [5.6.6. **OP 6**: Visualizazzione delle ultime 3 emissioni di un sensore](#566-op-6-visualizazzione-delle-ultime-3-emissioni-di-un-sensore)
		- [5.6.7. **OP 7**: visualizzare la catena dei sensori collegati fino al receiver](#567-op-7-visualizzare-la-catena-dei-sensori-collegati-fino-al-receiver)
- [6. Progettazione dell'applicativo](#6-progettazione-dellapplicativo)
	- [Descrizione dell’architettura dell’applicazione realizzata](#descrizione-dellarchitettura-dellapplicazione-realizzata)

# 1. Premessa
IOTDB è stato ideato e costruito per dimostrare le principali funzioni avanzate dei più comuni rdbms, non spiegate a lezione ma bensì studiate e approfondite in autonomia. Si è preferito quindi utilizzare una realtà relativamente poco complessa ma studiata appositamente per costruire diverse applicazioni e dimostrare tutte le potenzialità delle più recenti versioni di rdbms (MySQL 8.1 in questo caso), piuttosto che costruire realtà troppo complesse ma banali nel loro utilizzo. Tale decisione è rispecchiata e approfondita nell'aggiunta del capitolo: [5. Schema fisico](#5-schema-fisico).

# 2. Analisi dei requisiti
## 2.1. Intervista
> Si vuole realizzare una rete di sensori IOT, per il rilevamento e la misura di determinati ecosistemi naturali. I sensori verranno posizionati in diverse Area di particolare interesse. Saranno utilizzate due diverse tipologie di sensori con diversi obbiettivi. I Rilevatori avranno la funzione di allarme e produrranno un valore positivo solo nel caso sia rilevato un determinato evento naturale (es: rilevatori di terremoti, di incendi..). I misuratori quantificheranno una particolare caratteristica dell'ambiente circostante espressa attraverso una determinata unità di misura (es: misuratori di temperatura, di inquinamento sonoro..). Essendo dispositivi IOT, con la possibilità di essere locati in luoghi angusti e lontani saranno muniti di una batteria interna. Per comunicare i dati i sensori seguono i principali meccanismi delle reti IOT e si dovranno quindi collegare con il dispositivo IOT, libero e a sua volta collegato, più vicino formando così una catena di sensori. Ogni sensore emetterrà la propria informazione, correlata al momento della rilevazione, e stato della batteria, nel caso in cui questa sia carica e inoltrerà anche tutte le eventuali informazioni ricevute dal sensore vicino. Per collezzioanre i dati, un sensore ad una estremità della catena dovrà essere collegato a un ricevitore, posizionato uno per ogni zona in modo tale da aprovvigionarlo di una fonte di energia elettrica e un collegamento internet. Si vuole mantenere lo storico di tutte le informazioni ricevute dai sensori e avere la possibilità, attraverso questo di visualizzare le ultime informazioni emesse da ogni sensore. Si vuole inoltre potere visualizzare i vari colegamenti che sussitono da un certo sensore fino al ricevitore. Due sensori appartenenti a due Aree diverse e quindi distanti, non potranno essere collegati.


<!-- todo: forse questa parte va nella definizione delle specifiche -->
## 2.2. Rilevamento delle ambiguità e correzioni proposte
Oltre a chiarire eventuali ambiguità all'interno dell'intervista, viene proposta la traduzione dei possibili concetti principali individuati, utilizzate all'interno del codice di IOTDB per favorire la comprensione dell'implementazione a chiunque e saranno quindi utilizzate nel fasi successive dell'elaborato.
* La rete prevede 3 tipologie di componenti (*Component*): rilevatore (*Detector*), misuratore (*Meter*) e ricevitore (*Receiver*)
* Un sensore può essere collegato ad un altro sensore a sua volta collegato e libero, o ad un *Receiver* oppure non essere collegato a niente
* la connessione tra sensori è unidirezionale, dal sensore più lontano fino al ricevitore e non esiste nel verso contrario.
<span style="display:block;text-align:center;">![Test Automation](./assets/connection_schema.png)</span>
* Un sensore emette (*Emittion*) una informazione solo se collegato e la batteria è carica
* Lo storico mantiene tutti i dati correlati ai rispettivi sensori e al timestamp
* Visualizzare le ultime 3 emissioni per ogni sensore
* Solo i *Meter* utilizzano una unità di misura
* Può esserci al massimo un *Receiver* per ogni *Area*

### 2.2.1. Connettività e Libertà di un sensore
Si è voluto dedicare al lettore una sottosezione riguardante il concetto di *connettività* e *libertà* di un sensore, poichè ritenuto di fondamentale importanza per la comprensione della modalità in cui i componenti IOT si connettono e per chiarire in anticipo eventuali dubbi che potrebbero sorgere nella continuazione della lettura.

Un sensore all'interno di una area può essere collegato, e quindi trasmettere dati, o scollegato. Un sensore per essere collegato deve soddisfare i seguenti requisiti:
* **Connettività**: il componente deve essere a sua volta connesso tranne nel caso in cui sia un *Receiver*.
* **Libertà**: nessun altro sensore deve essersi già connesso a questo.

Queste due specifiche rendono possibile il concetto poco formale di "Catena di sensori" emerso nell'intervista. Inoltre garantiscono sempre l'esistenza di uno e un solo componente che soddisfi le due condizioni e sarà per forza il più recentemente aggiunto componente estremo della catena (se esistesse solo un componente nella catena allora questo dovrà essere per forza il *receiver*).

<span style="display:block;text-align:center;">![Test Automation](./assets/connection_options.png)</span>

## 2.3. Definizione delle specifiche ed estrazione dei concetti principali

Concetto | Specifica
-------- | -------------
*Component* | È una entità generica all'interno della rete. Può essere un *Receiver*, *Meter* o *Detector*. È caratterizzato da una descrizione e conserva lo stato della  batteria interna.
*Receiver* | Solo un Receiver può essere presente all'interno di una *Area*. Un sensore può essere collegato a questo per trasmettere un valore.
*Detector* | Un sensore rilevatore. Produce un valore positivo se si verifica un determinato evento.
*Meter* | Un sensore misuratore. Produce informazioni periodicamente attraverso un dato numerico. È caratterizzato da una unità di misura.
*Area* | Zona di particolare interesse. È caratterizzata da un nome.
*Emittion* | Corrisponde a una emissione di un sensore. È caratterizzata da un valore e da un timestamp.

Segue un elenco degli obbiettivi per cui il il sistema informativo sarà responsabile:
1. Visualizzazione delle aree disponibili
2. Visualizzazione dei componenti con le relative informazioni
3. Aggiunta di un sensore di tipo Meter
4. Aggiunta di un sensore di tipo Detector
5. Mantenere collegato o scollegato un detemrinato sensore
6. Ricezione e conservazione dello storico dei dati dei soli sensori connessi
7. Visualizzazione delle 3 ultime emissioni per ogni sensore
8. dato un certo sensore, visualizzare la catena dei sensori collegati a questo fino al receiver


# 3. Progettazione concettuale
## 3.1. Schema scheletro e raffinamenti proposti
### 3.1.1. Area e componente

<span style="display:block;text-align:center">![Test Automation](./assets/Area.JPG)</span>
L'*Area* è caratterizzata da un identificativo ed un nome. Ogni Componenete della rete, identificato anch'esso da un ID è collocato all'interno di una Area

### 3.1.2. Sensori
<span style="display:block;text-align:center">![Test Automation](./assets/Sensor.JPG)</span>

Ogni Componente della rete può essere o un sensore o un ricevitore. I sensori sono caratterizzati da una batteria interna e anch'essi possono essere o Misuratori o Rilevatori. I misuratori sono anche caratterizzati da una unità di misura. Ogni tipo di sensore può essere a sua volta di varie tipologie non specificate. Nello schema ne sono riportate alcune a scopo dimostrativo.

### 3.1.3. Connessione
<span style="display:block;text-align:center">![Test Automation](./assets/Connected.JPG)</span>

Ogni componente della rete può collegarsi ad un altro componente, che quindi può essere solo un sensore o un ricevitore. Come richiesto nella fase di analisi un componente può anche non essere connesso, di conseguenza la relazione Connected è quindi ad anello e la minima cardinalità a 0 rende la partecipazione ad essa non obbligatoria.

### 3.1.4. Emissione
<span style="display:block;text-align:center">![Test Automation](./assets/Data.JPG)</span>

Ogni sensore emette una informazione caratterizzata da un valore e il momento in cui è stata generata. Il timestamp e l'identificativo del sensore che ha prodotto l'informazione indicizzano ogni emissione all'interno dello storico.

## 3.2. Schema finale

<span style="display:block;text-align:center">![Test Automation](./assets/Final.JPG)</span>


# 4. Progettazione logica
## 4.1. Stima del volume dei dati
Si informa il lettore che i seguenti dati corrispondono a un mero esempio di come potrebbe essere il volume dei concetti presentati nell'arco di un anno di attività, di conseguenza sono stime fittizie ma proporzionalmente verosimili se presa in considerazione la *motivazione* correlata.
Concetto | Costrutto | Volume | Motivazione
---------|-----------|--------|------------
**Area**| E | 4 | Il volume di Area sarà notevolmente minore rispetto agli altri concetti
**Receiver** | E | 4 |  Un ricevitore per ogni Area*
**Meter**| E | 12 | 4 Misuratori per ogni Area
**Detector** | E | 8 | 2 Rilevatori per ogni Area
**Sensor** | E | 20 | 12 misuratori + 8 rilevatori
**Component**| E | 24 | 4 ricevitori, 12 misuratori, 8 rilevatori sparsi per 4 aree
*Collocated* | R | 24 | Ogni componente è posizionato all'interno di una area*
*Connected* | R | 16 | Alcuni sensori e tutte le antenne non saranno collegate ad un altro componente
*Emits* | R | 16 | Dopo un anno, si stima che tutti i sensori connessi abbiano prodotto almeno un'informazione
**Emittion** | E | 20000 | 48 * 365: se ogni misuratore collegato emette una informazione ogni mezz'ora, per un anno. Il volume è leggermente più alto per includere anche eventuali emissioni dai rilevatori. Contiene lo storico delle emissioni
(*: volume obbligatorio secondi i requisiti richiesti nella fase di analisi.)

## 4.2. Descrizione delle operazioni principali e stima della loro frequenza
La frequenza delle operazioni 1 e 2 è fortemente influenzata da quella delle 3 e 4. Infatti ogni volta che si aggiungerà un nuovo sensore sarà necessario decidere una zona di collocamento e un eventuale componente a cui connetterlo.
Codice | Operazione | Stima della frequenza
-|-|-
**1** | Visualizzazione delle aree disponibili | 50 volte all'anno
**2** | Visualizzazione dei componenti con le relative informazioni | 17000 volte all'anno
**3** | Data un'area, visualizazzione del possibile componente a cui un sensore potrebbe collegarsi | 50 volte all'anno
**4** | Aggiunta di un sensore di tipo Meter o Detector | 30 all'anno
**5** | Ricezione e conservazione nello storico di una emissione di un sensore | 20000 all'anno
**6** | Visualizazzione delle ultime 3 emissioni di un sensore |  12000 volte all'anno
**7** | visualizzare la catena dei sensori collegati fino al receiver | 750 volte all'anno

## 4.3. Schemi di navigazione e tabelle degli accessi
### 4.3.1. **OP 1**: Visualizzazione delle aree disponibili 
La visualizzazione delle aree necessita di un numero di letture proporzionale al volume dell'entità.
Concetto | Costrutto | Accessi | Tipo
-|-|-|-
Area | E | 4 | L
> **Totale**: 4L -> 50 volte all'anno

### 4.3.2. **OP 2**: Visualizzazione dei componenti con le relative informazioni 
La visualizzazione dei componenti necessita di un numero di letture proporzionale al volume dell'entità.
Concetto | Costrutto | Accessi | Tipo
-|-|-|-
Componenti | E | 24 | L
> **Totale**: 24L -> 17000 volte all'anno

### 4.3.3. **OP 3**: Data un'area, visualizazzione del possibile componente a cui un sensore potrebbe collegarsi
L'operazione consiste nella ricerca di un determinato componente all'interno di una certa area che soddisfa i requisiti descritti in [2.2.1. Connettività e Libertà di un sensore](#221-connettività-e-libertà-di-un-sensore).

L'operazione è effettuata eseguendo una lettura per ogni componente che soddisfa l'area all'interno di *Connected* (**Connettività**) e, una volta acquisito un probabile candidato, si verifica che lo stato della relazione *Connected* non contenga un riferimento al componente designato (**Libertà**).

Si esegueno 7 accessi su Collocated poichè si è stimato un numero di: 4 misuratori, 2 rilevatori e 1 ricevitore per ogni area. Di questi, solamente una parte saranno collegati (apparterranno a un possibile stato di *Connected*) perchè si è stimato un numero di 24 componenti, di cui 16 connessi (il 66%), perciò solamente 4,69 dei 7 componenti sarà in media connesso (il 66% di 7) e un possibile candidato.

	16/24 = 0,67 -> 
	7 * 0,67 = 4,69

Per ognuno di questi, è necessario soddisfare il criterio di libertà quindi si eseguirà una lettura intera dello stato di *Connected* per verificare se è refirito da qualche istanza (quindi qualche altro componente si è già collegato).

	4,69 * 16 = 75,04 ~= 75

**N.B.**: Per la [2.2.1. Connettività e Libertà di un sensore](#221-connettività-e-libertà-di-un-sensore), l'esito positivo della ricerca è sempre garantito.
Concetto | Costrutto | Accessi | Tipo
-|-|-|-
Area | E | 1 | L
Collocated | R | 7 | L
Connected | R | 75 L
> **Totale**: 82L -> 50 volte all'anno

<span style="display:block;text-align:center;width:80%">![Test Automation](./assets/Access_connected.png)</span>

### 4.3.4. **OP 4**: Aggiunta di un sensore di tipo Meter o Detector
Ogni sensore necessita di una area dove essere collocato. Questa deve essere un'area valida perciò è necessaria una lettura delle aree: [**OP 1**](#431-op-1-visualizzazione-delle-aree-disponibili). Inoltre nel caso si voglia connettere un sensore ad un componente sarà necessario eseguire l'[**OP 2**](#432-op-2-data-unarea-visualizazzione-del-possibile-componente-a-cui-un-sensore-potrebbe-collegarsi).

Concetto | Costrutto | Accessi | Tipo | Opzionale
-|-|-|-|-
OP 1 | E | 4 | L | No
Collocated | R | 1 | S | No
Component | E | 1 | S | No
Meter o Detector | E | 1 | S | No
OP 2 | Mista | 82 | L | Si
Connected | R | 1 | S | Si
> **Totale**: 4S, 86L -> 20 volte all'anno

### 4.3.5. **OP 5**: Ricezione e conservazione nello storico di una emissione di un sensore
Concetto | Costrutto | Accessi | Tipo
-|-|-|-
Emittion | E | 1 | S
Emits | R | 1 | S
> **Totale**: 2S -> 20000 volte all'anno

### 4.3.6. **OP 6**: Visualizazzione delle ultime 3 emissioni di un sensore
Sarà necessario prelevare, in ordine di timestamp più recente, tutte le emissioni del sensore richiesto dalla entità *Emittion*, che contiene lo storico delle emissioni, e eseguire 3 letture.
Concetto | Costrutto | Accessi | Tipo
-|-|-|-
Emittion | E | 20003 | L
> **Totale**: 20003L -> 1200 volte all'anno

### 4.3.7. **OP 7**: visualizzare la catena dei sensori collegati fino al receiver
Dato un sensore, bisogna ripercorrere la catena di collegamento attraverso la relazione *Connected*. Il numero degli accessi sarà equivalente al numero medio di componenti connessi per area, a partire da un componente casuale.

	4,69/2 = 2,345
	
Concetto | Costrutto | Accessi | Tipo
-|-|-|-
Connected | R | 2,345 | L
> **Totale**: 2,345L -> 750 volte all'anno

## 4.4. Raffinamento dello schema
### 4.4.1. Eliminazione delle gerarchie
All'interno dello schema è presente una gerarchia a doppio livello di *Component*: prima si divide in *Sensor* e *Receiver* e poi *Sensor* a sua volta in *Meter* e *Detector*. Si è deciso di attuare un collasso verso l'alto completo con le seguenti caratteristiche:
* Aggiunta di un campo Enum di nome "Kind" con valori: "Meter", "Detector", "Receiver"
* Il campo unit non obbligatorio

<span style="display:block;text-align:center;">![](./assets/Component_logic.JPG)</span>

È stata presa la decisione di non mantenere *Component* e *Sensor* come due entità separate poichè la maggior parte dello stato di *Component* sarà costituito da elementi di *Sensor* (in quanto il volume di *Sensor* è decisiamente più elevato rispetto a quello di *Receiver*) ed inoltre avrebbe complicato le operazioni di base come l'aggiunta o la ricerca di un sensore in una determinata area. Il collasso verso il basso non è stato considerato in quanto avrebbe reso impossibile la corretta traduzione della relazione *Connected*.

### 4.4.2. Eliminazione degli identificatori esterni
Nello schema E/R sono eliminate le seguenti relazioni:
* *Collocated*: importanto <ins>ID</ins> di *Area* all'interno di *Component*
* *Connected*: importanto <ins>ID</ins> di *Component* all'interno di *Component* (Un componente è connesso con un altro componente)
* *Emits*: importanto <ins>ID</ins> di *Component* all'interno di *Emittion*

### 4.4.3. Scelta delle chiavi primarie
Per le entità *Area* e *Component* è stata scelta un chiave primaria auto incrementante di nome <ins>ID</ins>. Per quanto riguarda l'entità *Emittion* ogni emissione conservata nello storico sarà identificata dal sensore che l'ha prodotta (chiave importata di *Component*) e il momento in cui è stata prodotta (timestamp).

## 4.5. Schema relazione finale
<span style="display:block;text-align:center;">![](./assets/logic_final.JPG)</span>

# 5. Schema fisico
## 5.1. Introduzione
Si è da sempre voluto distiguere questo elaborato dagli altri e ciò è stato realizzato con il concetto di "live data" o "dati vivi". Invece che popolare il database con dati inventati e costruiti su misura per dimostrare le funzionalità dell'applicativo, si è preferito delegare il sistema informativo di generare automaticamente i dati in tempo reale da utilizzare all'interno dell'applicativo. Questo per dimostrare le potenzialità dei più recenti rdbms, attraverso costrutti avanzati approfonditi autonomamente, e creare nell'utente l'impressione di interfacciarsi con un "vera" base di dati dove le informazioni sono sempre nuove e diverse da quelle precendenti. Il "live data" e altre concetti utilizzati sono implementati attraverso *Stored Objects*, come Views, Stored Procedures e Events, di seguito approfonditi.

## 5.2. Views
Le viste sono utilizzate dall'applicativo per le interrogazioni più comuni e per rendere possibile l'*indipendenza logica* dei dati. IOTDB utilizza 2 View principali.
### 5.2.1. component_info
component_info è una vista utilizzata per reperire tutti componenti con le relative informazioni:
```sql
CREATE VIEW component_info AS
	SELECT Component.ID, Component.Description, Component.Kind, Component.Area, Component.Battery
	FROM Component;
```

### 5.2.2. area_info
area_info è una vista utilizzata per reperire tutte le aree con le relative informazioni:
```sql
CREATE VIEW area_info AS
	SELECT *
	FROM Area;
```
## 5.3. Stored procedures
Le *stored procedures* sono utilizzate per rendere più immediate, facili e parametriche le interrogazioni più complesse che necessita IOTB. Si utilizzano 3 stored procedures principali.

### 5.3.1. last_emittions
Questa procedura aggiunge allo schema logico una tabella temporanea, interrogabile e consultabile, all'interno della base di dati e la popola con le ultime n emissioni di tutti i sensori. La tabella di nome "last_emittions" è costruita mediante una complessa interrogazione che utilizza un natural join tra Component e un'altra tabella temporanea generata sul momento all'interno della query di nome "**x**". 

All'interno della clausola ```FROM``` viene costruita **x** formata da ogni emissione di ogni sensore e per ognuna di queste un campo correlato "rnk" generato dalla funzione MySQL ```RANK()``` che, raggruppando per ogni sensore, genera un "valore" tanto minore quanto recente è l'emissione. In pratica si ottengono tutte le emissioni, per ogni sensore, correlate con il loro numero di ordine. 

Una volta ottenuta **x**, si filtrano solo le emissioni con un numero di ordine < di quello dato (in questo caso n). Si è poi sfruttato il join con la tabella Component per selezionare solo i sensori e l'eventuale unità di misura.

```sql
DELIMITER $$

#Prende i più recenti N dati da ciascun sensore Meter o Detector
CREATE PROCEDURE last_emittions(in n integer)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK; 
		RESIGNAL;
	END;

	START TRANSACTION;
	DROP TEMPORARY TABLE IF EXISTS last_emittions;
	CREATE TEMPORARY TABLE last_emittions(
		SensorID int not null,
		Emittion_Time datetime not null default NOW(),
		Emittion_Value int not null,
		Unit varchar(3),
		primary key (SensorID, Emittion_Time)
	) ENGINE=MEMORY AS (
		SELECT SensorID, Emittion_Time, Emittion_Value, Unit
		FROM (
			SELECT *, RANK() OVER (PARTITION BY SensorID ORDER BY Emittion_Time DESC) AS rnk
			FROM Emittion
		) AS x, Component
		WHERE rnk <= n AND SensorID = Component.ID AND (Kind="Meter" OR Kind="Detector")
	);
	COMMIT;
	SELECT * FROM last_emittions;
END $$
```
Data la portata della *sotered procedure*, al suo interno è presente un controllo per eventuali errori con ROLLBACK delle modifiche e emissione dell'errore. È anche presente l'istruzione "```START TRANSACTION```" che segna l'inizio di una transazione atomica all'interno della base di dati, questo per mantenere l'integrità dei dati e il rispetto delle regole *ACID*.

### 5.3.2. sensor_emit
Questa procedura si occupa di generare una nuova emissione per ogni sensore collegato. È utilizzata per costruire il concetto di "live data". Ogni valore di ogni nuova emissione, relativamente a un certo sensore, è basato sull'ultimo precedentemente emesso o, in caso non abbia mai prodotto un'emissione prima, il valore è generato ex-novo. In questo modo, tutti sensori genereranno una emissione, anche quelli nuovi appena aggiunti. 

**N.B.**: *sensor_emit* sfrutta a sua volta la procedura "last_emittions" descritta nel capitolo precendente. Di conseguenza va utilizzata sinergicamente con "last_emittions" invocando prima l'istruzione "last_emittions(1)", per ottenere l'ultima emissione di ogni sensore, dopodichè chiamare "sensor_emit()". Purtroppo la struttura e sintassi delle procedure dell'rdbms MySQL non permette che una procedura chiami un'altra dopo la dichiarazione delle variabili.

Per ogni sensore, la procedura percorre, attraverso l'utilizzo di un cursor, una tabella temporanea costruita attraverso un join tra Component e la tabella temporanea last_emittions e filtrando i soli sensori. In questo modo si otterranno tutti i sensori con la loro ultima emissione.

La procedura produce nuove emissioni solo per i sensori connessi e carichi.

Se il sensore aveva già prodotto un valore, il valore della nuova emissione si baserà su questo, mentre se il sensore è nuovo si genera una nuova emissione con un valore casuale. Per i rilevatori invece, indipendentemente dall'ultima emissione, si utilizza un valore causale tra 0 e 1, generato ad ogni iterazine, che se maggiore di una certa soglia e quindi con una certa probabilità, permetterà l'emissione di un valore positivo.


```sql
/*
Emette un nuovo valore per i sensori che sono connessi e che sono 'Meter' o 'Detector'.
Se il sensore è di tipo meter ed è nuovo, allora immette un valore tra 20 e 30 
altrimenti emette l'ultimo valore emesso da quel sensore +- 4.
Se il sensore è di tipo detector ci sono 3/10 di possibilità di emettere un valore positivo.
*/
CREATE PROCEDURE sensor_emit()
BEGIN
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE SensorID INT;
    DECLARE Connected INT;
	DECLARE Kind ENUM('Meter', 'Detector');
    DECLARE Battery INT;
	DECLARE Emittion_Value INT;
    DECLARE new_value INTEGER;
	DECLARE sensor_last_emit_cursor CURSOR FOR 
		SELECT Component.ID, Component.Connected, Component.Battery, Component.Kind, last_emittions.Emittion_Value 
		FROM Sensor LEFT OUTER JOIN last_emittions ON Sensor.ID = last_emittions.SensorID
		WHERE Sensor.Kind = 'Meter' OR Sensor.Kind = 'Detector';
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

	OPEN sensor_last_emit_cursor;
	sensor_last_emit_loop: LOOP
		FETCH sensor_last_emit_cursor into SensorID, Connected, Battery, Kind, Emittion_Value;
		IF finished = 1 THEN 
			LEAVE sensor_last_emit_loop;
		END IF;
		IF Connected IS NOT NULL AND Battery > 0 THEN
			IF Kind = 'Meter' THEN
				IF Emittion_Value IS NULL THEN
						INSERT INTO Emittion VALUES (SensorID, NOW(), FLOOR(RAND()*(30-20+1))+20);
				ELSE
						INSERT INTO Emittion VALUES (SensorID, NOW(), Emittion_value+4*FLOOR(RAND()*(1+1+1)-1));
			END IF;
			ELSEIF Kind = 'Detector' AND RAND() >= 0.7 THEN
				INSERT INTO Emittion VALUES (SensorID, NOW(), 1);
			END IF;
		END IF;
	END LOOP;
END $$
```

### 5.3.3. component_path
La procedura component_path si occupa di recuperare il cammino, all'interno della "catena di componenti", che lega un componente dato al suo receiver, utilizzando come guida il campo "connected".

Sfrutta le interrogazioni ricorsive, una funzionalità avanzata di MySQL introdotta dalla versione 8.0

L'interrogazione ricorsiva è formata da due parti:
1. La prima è una interrogazione di inizio chiamata "anchor member", che dato un input produce un determinato output set
2. La seconda query è detta "recursive member" e non fa altro che utilizzare l'output dell'anchor member per generare un nuovo set

L'interrogazione opera nel seguente modo:
1. Genera il set base con l'anchor member utilizzando il parametro passato alla procedura
2. Il recursive member utilizza l'anchor output i per generare un input i + 1
3. L'anchor member prenderà come input questa volta l'output i+1 generato dal recursive member
4. Ogni output set dell'anchor member viene unito attraverso l'istruzione di UNION fino a che la recursive member non produce un output nullo

Si visualizzano infine, partendo dal sensore dato, alcune informazioni e il campo connected fino al ricevitore.

```sql
CREATE PROCEDURE component_path (in initial_sensor_id integer)
	WITH RECURSIVE c_path AS(
		SELECT ID, Description, Connected
		FROM Sensor
		WHERE ID = initial_sensor_id
		UNION ALL
		SELECT s.ID, s.Description, s.Connected
		FROM Component c INNER JOIN c_path cp ON cp.Connected = c.ID)
	SELECT *
	FROM c_path;
```

## 5.4. Events
Gli eventuali sono utilizzati dall'rdbms come parte integrante nell'implementazione del concetto di "live data".

### 5.4.1. sensor_periodic_discharge_event
Questo evento è utilizzato per diminuire la batteria di tutti i sensori. I sensori con la batteria scarica non potranno più emettere dati. L'evento viene invocato automaticamente dall'rdbms una volta al giorno e diminiusce ogni volta la batteria di un punto percentuale attraverso una query di UPDATE.

I ricevitori sono esclusi dall'azione della query poichè sono provvisti di una fonte di energia elettrica come emerso nella fase di intervista, di conseguenza la loro batteria non calerà mai.
```sql
#Diminuisce di un punto percentuale tutti i componenti che non sono 'receiver' ogni giorno
CREATE EVENT sensors_periodic_discharge_event
	ON SCHEDULE EVERY 1 DAY
	DO
		UPDATE Sensor SET Battery = Battery - 1 WHERE Kind <> 'Receiver';
```
### 5.4.2. sensor_periodic_emittions_event
*sensor_periodic_emittions_event* è il tassello finale nella realizzazione del concetto di "live data". Si occupa di generare nuovi dati per ogni sensore ogni 5 minuti.

L'evento utilizza una combinazione delle due procedure [last_emittions](#531-last_emittions) e [sensor_emit](#532-sensor_emit), utilizzate in modo sinergico e coordinato per la corretta realizzazione dell'obbiettivo.

```sql
# genera nuove emissioni per i sensori ogni 5 minuti
CREATE EVENT sensors_periodic_emittions_event
     ON SCHEDULE EVERY 5 MINUTE
     DO
		BEGIN
			CALL last_emittions(1);
			CALL sensor_emit();
		END $$
```

## 5.5. Triggers
È stato impiegato anche un trigger per preservare la giusta immissione di nuovi dati, evitando banali controsensi prodotti da un errore di distrazione. 

Si è pensato necessario l'utilizzo di *trigger* per il controllo dell'input, poichè i CHECK CONSTRAINT, introdotti durante la definizione delle tabelle dello schema logico, operano a livello di tupla e non a livello di tabella. Ciò non permette di attuare controlli come l'esistenza di un ricevitore in una determinata area al momento dell'aggiunta di un altro.

I trigger vengono automaticamente invocati prima di una determinata operazione (insert, delete, update) all'interno di una determinata tabella. Nel caso l'operazione, con i relativi dati, non soddisfacesse i criteri interni del trigger, questa viene rifiutata e viene generato un errore con un rispettivo codice.

Il codice di errore utilizzato dai trigger di IOTDB è il 45000 che, nel caso di MySQL, corrsiponde a un eccezzione genenerica definita dal programmatore.
>"To signal a generic SQLSTATE value, use '45000', which means “unhandled user-defined exception.”
> -- <cite>[MySQL 8.0 Reference Manual](https://dev.mysql.com/doc/refman/8.0/en/signal.html) </cite>


**N.B.**: È condiviso nella comunità di esperti di rdbms che l'utilizzo dei trigger, seppur comodo e immediato, risulta essere pericoloso in quanto questi non generano un errore, se non espressamente richiesto all'interno della loro implementazione e quindi questo può portare a dei risultati inspiegabilmente diversi dopo determinate operazioni con dati errati. Inoltre si possono generare solo erorri attraverso un codice, tra quelli forniti dal rdbms in uso, ma ciò fornisce solo un output preciso di un errore corrispondente al codice utilizzato ma non facilita l'individuazione del trigger che l'ha scaturito. Nel caso si avessero 2 trigger diversi con controlli diversi, non si potrebbe individuare quale specifica sia stata violata.

### 5.5.1. connected_trigger
Questo trigger rileva possibili errate immissioni di nuovi sensori nel caso questi fosserro connessi con un altro componente non residente nella medesima area vedi: [2.2. Rilevamento delle ambiguità e correzioni proposte](#22-rilevamento-delle-ambiguità-e-correzioni-proposte).

```sql
# Trigger che previene l'inserimento di un sensor di una Area X collegato a un sensore di una area Y
create trigger connected_trigger
before insert on Component
for each row 
begin
    DECLARE connected_Area INT;
    
    if new.Connected is not null then
		SELECT Area into connected_Area
		From Sensor
		WHERE ID = new.Connected;
		
		if new.Area <> Connected_Area then
			signal sqlstate '45000';
		end if;
	end if;		
END $$
```

### 5.5.2. receiver_trigger
```sql
# Trigger che rileva se si sta inserendo un receiver in una area nel caso ne esista già uno
create trigger receiver_trigger
before insert on Component
for each row 
begin
    DECLARE receiver INT;
    
    if new.Kind = "receiver" then
		SELECT ID into receiver
		From Component
		WHERE Kind = "receiver"
		AND Area = new.Area;
		
		if receiver is not null then
			signal sqlstate '45000';
		end if;
	end if;		
END $$
```

## 5.6. Traduzione delle operazioni in query sql
### 5.6.1. **OP 1**: Visualizzazione delle aree disponibili 
```sql
SELECT * FROM area_info; 
```

### 5.6.2. **OP 2**: Visualizzazione dei componenti con le relative informazioni
```sql
SELECT * FROM component_info; 
```

### 5.6.3. **OP 3**: Data un'area, visualizazzione del possibile componente a cui un sensore potrebbe collegarsi
La prima condizione all'interno del predicato di where filtra i sensori che non appartengono all'area data.

La seconda soddisfa la condizione di ***libertà*** di un componente: *Un componente che non è referenziato da nessuno in Connected, è un componente libero ovvero che nessun'altro componente si è connesso*.

Mentre la terza soddisfa quella di ***connettività***: *un sensore deve essere connesso o deve essere un Receiver*
```sql
SELECT ID
FROM Component 
WHERE Area = ?
AND ID NOT IN (
	SELECT Connected
	FROM Component
	WHERE Connected IS NOT NULL)
AND ((Connected IS NOT NULL) OR (Connected IS NULL AND Kind = 'Receiver'))
```

### 5.6.4. **OP 4**: Aggiunta di un sensore di tipo Meter o Detector
**Meter**:
```sql
INSERT INTO Component (Connected, Battery, Description, Area, Kind, Unit) VALUES (?, ?, ?, ?, "Meter", ?);
```
**Detector**:
```sql
INSERT INTO Component (Connected, Battery, Description, Area, Kind, Unit) VALUES (?, ?, ?, ?, "Detector", NULL)
```

### 5.6.5. **OP 5**: Ricezione e conservazione nello storico di una emissione di un sensore
Questa operazione è effettuata all'interno dell'evento [sensor_periodic_emittions_event](#542-sensor_periodic_emittions_event).
```sql
CALL last_emittions(1);
CALL sensor_emit();
```

### 5.6.6. **OP 6**: Visualizazzione delle ultime 3 emissioni di un sensore
Questa operazione è effettuata dalla stored procedure [last_emittions](#531-last_emittions).

```sql
CALL last_emittions(3);
```
### 5.6.7. **OP 7**: visualizzare la catena dei sensori collegati fino al receiver
Questa operazione è effettuata dalla stored procedure [component_path](#533-component_path).
```sql
CALL component_path();
```


# 6. Progettazione dell'applicativo
## Descrizione dell’architettura dell’applicazione realizzata
Si può consultare e gestire IOTDB attraverso la rispettiva applicazione web creata.

![](./assets/IOTDB.JPG)

L'applicazione è stata scritta in PHP e Javascript. È completamente responsive e reattiva grazie all'uso di AJAX.

Si collega autenticandosi al database locale e utilizza tutte le operazioni principali per richiedere le principali informazioni come le aree, i sensori e le rispettive emissioni. Ad ogni operazione (come l'aggiunta di un nuovo sensore) l'applicazione si aggiorna automaticamente con i nuovi dati. 

L'operazione di aggiunta di un nuovo sensore è controllata e verificata. Se si vuole connettere il sensore, l'applicazione mostra subito quale sarà l'id del sensore a cui ci si collegherà, altrimenti si può decidere di lasciarlo scollegato attraverso un comodo switch.