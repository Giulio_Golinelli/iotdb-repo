//popola il selettore di aree
async function get_area(){
	await $.get("/php/get_area.php", function(data){
		$("#input_area").empty();
		data.forEach(element => {
			$("#input_area").append(`<option value="${element.ID}">${element.Name}</option>`)
		});
	}, "json");
}

//popola la colonna con le informazioni di base di tutti i sensori
async function get_sensor_info(){
	await $.get("/php/sensor_info.php", function(data){
		sensors = data;
		data.forEach(element => {
			let sensor_card = `<div id="${element.ID}" class="card bg-light">
				<div class="card-body">
				<div class="row">
					<div class="col my-auto">
						<div class="row">
							<h4 id="sensor_id" class="card-text px-1">#${element.ID}</h4>
							<h4 id="description" class="card-text px-1">${element.Description}
						</div>
						<div class="row">
							<h4>
								<span id="type" class="badge badge-pill badge-primary">${element.Kind}</span>
								<span class="badge badge-pill badge-info">Area: ${element.Area}</span>
							</h4>
						</div>
					</div>
					<div class="col-3 text-center my-auto">
						<h6>Battery:</h6>
						<h4 id="battery" class="text-success">${element.Battery}%</h4>
					</div>
				</div>
				<div class="row">
					<div id="last_values_emitted" class="col">
					</div>
				</div>
			</div>
			<div class="card-footer text-center" data-toggle="collapse" data-target="#collapse${element.ID}">
				<span class="path-button btn-block">Path</span>
				<div id="collapse${element.ID}" class="collapse">
				</div>
			</div>
		</div>`;
			$("#sensors_column").append(sensor_card);
		});
	}, "json");
}

//popola ogni sensore con gli ultimi suoi 3 valori, al massimo, emessi
async function last_emittions(){
	await $.get("/php/last_emittions.php", function(data){
		data.forEach(element => {
			//$("#"+element.SensorID+" #last_values_emitted").empty();
			if($("#"+element.SensorID+"  #last_values_emitted").children().length == 0)
				$("#"+element.SensorID+" #last_values_emitted").append('<h6  class="text-center">Last values emitted:</h6>');
			var values = `<div class="row">
				<div class="col text-center">
					<td>${element.Emittion_Time}</td>
				</div>`;
			if(element.Unit != null) 
				values += `<div class="col text-center">
					<td>
						<span id="value">${element.Emittion_Value}</span>
						<span id="unit" class="font-weight-bold">${element.Unit}</span>
					</td>
				</div>
			</div>`;
			$("#"+element.SensorID+" #last_values_emitted").append(values);
		});
	}, "json");
}

async function connections(){
	await sensors.forEach(sensor => {
		$.get("/php/sensor_path_find.php", {sensor_id: sensor.ID}, function(data){
			let connection = "";
			if(data.length <= 1)
				connection = "<div class='row'><div class='col'>This sensor is not connected to anything</div></div>";
			else {
				connection += `<div class='row'>
					<div class='col'>sensor ID:</div>
					<div class='col'>connected to:</div>	
				</div>`;
				data.forEach(connected_sensor => {
					connection += `<div class="row">
						<div class="col">${connected_sensor.ID}</div>
						<div class="col">${connected_sensor.Connected}</div>
					</div>`
				});
			}
			//$("#collapse"+sensor.ID).empty();
			$("#collapse"+sensor.ID).append(connection);
		},"json");
	});
}

function get_sensor_in_area(area){
	$.get("/php/sensor_in_area.php", { area: $("#input_area").find("option:selected").attr('value')}, function(data){
		$("#input_connected_output").html("<b>Connected to #"+data.ID+"</b>");
		$("#input_connected_input").val(data.ID);
	}, "json");
}

async function reload(){
	$("#sensors_column").children().not(':first-child').remove();
	await get_area();
	await get_sensor_info();
	await last_emittions();
	await connections();
}

var sensors;

$(document).ready(async function() {
	$('#input_kind').change(() => {
		let x = $("#input_kind").find("option:selected").attr('value') == "Detector";
		if(x) 
			$("#input_unit").val("");
		$("#input_unit").prop('disabled', x);
		$("#input_unit").prop('required', !x);
	});

	$('#input_connected').change(function(){
		if($(this).is(":checked"))
			get_sensor_in_area();
		else {
			$("#input_connected_output").html("<b>Not connected</b>");
			$("#input_connected_input").val(0);
		}
	});

	$('#new_sensor_form').submit(function(e){
		e.preventDefault();
		console.log($('#new_sensor_form').serialize());
		$.get({
			url:$('#new_sensor_form').attr('action'),
			type:'get',
			data:$('#new_sensor_form').serialize(),
			success: function(data){
				reload();
			}
		});
	});
	
	reload();
});