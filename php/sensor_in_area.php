<?php
	include "connect.php";

	if(!isset($_GET['area']))
		$result = "not valid area";
	else {
		$query = mysqli_query($con, "SELECT ID
									FROM Sensor 
									WHERE Area = ".$_GET['area']."
									AND ID NOT IN (
										SELECT Connected
										FROM Sensor
										WHERE Connected is not NULL)
									AND ((Connected IS NOT NULL) OR (Connected IS NULL AND Kind = 'Receiver'))");
		$row = mysqli_fetch_assoc($query);
	}
	echo json_encode($row);
	mysqli_close($con);