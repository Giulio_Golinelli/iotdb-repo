<?php
	include "connect.php";

	if(!isset($_GET['sensor_id']))
		$result = "not valid sensor id";
	else {
		$query = mysqli_query($con, "CALL sensor_path(".$_GET['sensor_id'].")");
		while($row = mysqli_fetch_assoc($query))
			$result[] = $row;
	}
	echo json_encode($result);
	mysqli_close($con);